FROM golang:1.13.4-alpine AS builder

RUN apk --no-cache add git gcc
COPY . /src

RUN cd /src && go build -o /forms-ws-notifications main.go

FROM alpine:3.10

ARG NOTIFICATION_SERVER_PORT

RUN apk add --no-cache ca-certificates

COPY --from=builder /forms-ws-notifications /forms-ws-notifications

EXPOSE $NOTIFICATION_SERVER_PORT
CMD [ "/forms-ws-notifications" ]