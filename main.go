package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
	"gitlab.com/it.devel.att/go-ws-notifications/config"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Report struct {
	Id     string `json:"id"`
	Number int64  `json:"number"`
}

func init() {
	if err := godotenv.Load(".env.example"); err != nil {
		log.Print("No .env file found")
	}
}

func main() {
	config := config.New()

	rdb := redis.NewClient(&redis.Options{
		Addr:     config.Redis.HostPortString(),
		Password: config.Redis.Password,
		DB:       config.Redis.DB,
	})
	log.Println(config.Redis.HostPortString())
	log.Println(config.Redis.Password)
	log.Println(config.Redis.DB)

	ctx := context.Background()

	http.HandleFunc("/ws/api/v1/questionnaire", func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
		conn, err := upgrader.Upgrade(w, r, nil)

		if err != nil {
			log.Printf("Error while upgrade connection %v", err)
			return
		}
		log.Printf("New connection from %v address", conn.RemoteAddr().String())
		questionnaireId := r.URL.Query().Get("id")

		if questionnaireId == "" {
			log.Printf("questionnaireId is empty")
			return
		}

		defer conn.Close()

		var redisChannel string = fmt.Sprintf("questionnaire:%v:report:new", questionnaireId)
		pubsub := rdb.Subscribe(ctx, redisChannel)

		for {
			log.Printf("Connection %v address", conn.RemoteAddr().String())

			msg, err := pubsub.ReceiveMessage(ctx)
			if err != nil {
				log.Printf("Error while get message from redis channel %v | err = %v", redisChannel, err)
				return
			}

			var report Report
			err = json.Unmarshal([]byte(msg.Payload), &report)

			if err != nil {
				log.Printf("Error while serialize json from channel. msg.Payload = %v", msg.Payload)
				return
			}
			if err = conn.WriteMessage(websocket.TextMessage, []byte(msg.Payload)); err != nil {
				log.Printf("Error while send message to connection, err = %v", err)
				return
			}
		}
	})

	err := http.ListenAndServe(fmt.Sprintf(":%v", config.Server.Port), nil)
	if err != nil {
		panic(err)
	}
}
