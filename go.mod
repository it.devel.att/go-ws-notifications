module gitlab.com/it.devel.att/go-ws-notifications

go 1.13

require (
	github.com/go-redis/redis/v8 v8.0.0-beta.12
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
)
