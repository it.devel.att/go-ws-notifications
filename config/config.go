package config

import (
	"fmt"
	"os"
	"strconv"
)

type RedisConfig struct {
	Host     string
	Port     string
	DB       int
	Password string
}

func (rc *RedisConfig) HostPortString() string {
	return fmt.Sprintf("%v:%v", rc.Host, rc.Port)
}

type ServerConfig struct {
	Port string
}

type Config struct {
	Redis  RedisConfig
	Server ServerConfig
}

func New() *Config {
	return &Config{
		Redis: RedisConfig{
			Host:     getEnv("REDIS_HOST", "localhost"),
			Port:     getEnv("REDIS_PORT", "6379"),
			DB:       getEnvAsInt("REDIS_DB", 0),
			Password: getEnv("REDIS_PASSWORD", ""),
		},
		Server: ServerConfig{
			Port: getEnv("NOTIFICATION_SERVER_PORT", "9090"),
		},
	}
}

func getEnv(key string, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultValue
}

func getEnvAsInt(name string, defaultValue int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}
	return defaultValue
}
